package entitie;

import service.FilmeSet;

import java.util.List;

public class Filme implements FilmeSet {
	
	private String nome;
	
	public Filme() {
		
	}

	public Filme(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean contains(List<Filme> filme, Filme filme1) {
		boolean consultar;
		consultar = filme.contains(filme1);
		return consultar;
	}

	@Override
	public boolean add(List<Filme> filme, Filme filme1) {
		boolean adicionar;
		adicionar = filme.add(filme1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Filme> filme, Filme filme1) {
		boolean remover;
		remover = filme.remove(filme1);
		return remover;
	}

	@Override
	public int size(List<Filme> filme) {
		return filme.size();
	}

	@Override
	public void clear(List<Filme> filme) {
		filme.clear();
	}
	
}
