package main;

import entitie.Filme;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {
	
	static List<Filme> filme = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		String nome, r;
		Filme f = new Filme();
		
		char escolha = 's';
		while(escolha == 's') {
			System.out.println("Informe o nome do filme que deseja salvar: ");
			nome = sc.nextLine();
			
			f = new Filme();
			f.add(filme, f);
			System.out.println("Deseja salvar outro filme? s - sim   n - n�o");
			escolha = sc.nextLine().charAt(0);
		}
		
		System.out.println("Deseja consultar os filmes? s - sim   n - n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			String teste = " ";
			if (f.contains(filme, f)) {
				teste = "existem.";
			} else {
				teste = "n�o existem.";
			}
			System.out.println("Os filmes " + teste);
		}

		System.out.println("Deseja remover um filme? s - sim   n - n�o");
		r = sc.nextLine();
		if(r.equals("s")) {
			System.out.println(f.remove(filme, f));
		} 
		
		System.out.println("A quantidade de filmes � " + f.size(filme));
		
		System.out.println("Deseja limpar a lista de filmes? s - sim   n - n�o");
		r = sc.nextLine();
		if(r.equals("s")) {
			f.clear(filme);
			System.out.println("A quantidade de filmes agora � " + f.size(filme));
		} else {
			System.out.println("A quantidade de filmes � " + f.size(filme));
		}
	}

}
