package service;

import entitie.Filme;

import java.util.List;

public interface FilmeSet {

	public boolean contains(List<Filme> filme, Filme filme1);

	public boolean add(List<Filme> filme, Filme filme1);

	public boolean remove(List<Filme> filme, Filme filme1);

	public int size(List<Filme> filme);

	public void clear(List<Filme> filme);
}
